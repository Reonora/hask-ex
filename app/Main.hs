module Main where

import Lib3
import Data.List (inits)
import Data.Functor
import Zad2
import Zad3

main :: IO ()
main = print $ readInts2 "1 23 456 79"