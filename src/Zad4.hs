module Zad4 where

data Exp
  = EInt Int             -- stała całkowita
  | EAdd Exp Exp         -- e1 + e2
  | ESub Exp Exp         -- e1 - e2
  | EMul Exp Exp         -- e1 * e2
  | EVar String          -- zmienna
  | ELet String Exp Exp  -- let var = e1 in e2

instance Show Exp where
  show(EInt a) = show a
  show(EAdd a b) = show a ++ " + " ++ show b
  show(ESub a b) = show a ++ " - " ++ show b
  show(EMul a b) = show a ++ " * " ++ show b
  show(EVar a) = show a
  show (ELet s e1 e2) = "let " ++ show s ++ " = " ++ show e1 ++ " in " ++ show e2

instance Eq Exp where
 (==) (EInt a) (EInt b) = a == b
 (==) (EAdd a c) (b) = (a + c) == b