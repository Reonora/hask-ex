module Zad2 where

import Prelude hiding(Either(..))
data Either e a = Left e | Right a

instance Functor (Either e) where
  -- fmap :: (a -> b) -> Either e a -> Either e b
  fmap f (Left a) = Left a
  fmap f (Right a) = Right (f a)


{-
reverseRight :: Either e [a] -> Either e [a]
reverseRight (Left e) = Left e
reverseRight (Right []) = Right []
reverseRight (Right (x:xs)) = reverseRight (Right (xs)) ++ Right([x])-}

class Functor f => Pointed f where
  pure :: a -> f a

instance Pointed [] where
  pure a = [a]
