module Zad3 where

import Data.Char

readInts :: String -> [Int]
readInts s = map read (filter (all (isDigit)) (words s))

notDigits :: String -> Bool
notDigits s = length (filter (all (isDigit)) (words s)) /= length (words s)

readInts2 :: String -> Either String [Int]
readInts2 s = if notDigits s
            then (Left "buont")
            else Right(readInts s)