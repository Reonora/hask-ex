module Lib3 where

data Tree a = Empty | Node a (Tree a) (Tree a) deriving (Show)
-- lewo: mniejsze. prawo: wieksze
instance Eq a => Eq (Tree a) where
      (==) (Empty) (Empty) = True
      (==) (Empty) (Node a b c) = False
      (==) (Node a b c) (Empty) = False
      (==) (Node a b c) (Node d e f) = a == d && b == e && c == f

insert :: (Ord a) => a -> Tree a -> Tree a
insert a Empty = Node a Empty Empty
insert a (Node x left right) = if a <= x
  then insert a left
  else insert a right

contains :: (Ord a) => a -> Tree a -> Bool
contains a Empty = False
contains a (Node x left right) = if a < x
  then contains a left
  else contains a right

fromList :: (Ord a) => [a] -> Tree a
fromList [] = Empty
fromList (x:xs) = insert x (fromList xs)

{-
fromList :: (Ord a) => [a] -> Tree a-}
