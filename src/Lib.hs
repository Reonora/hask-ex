module Lib
    where

someFunc :: IO ()
someFunc = putStrLn "hey"

triples :: Int -> [(Int,Int,Int)]
triples n = [(a, b, c) | a <- [1..n], b <- [1..n], c <- [1..n]]

triads :: Int -> [(Int,Int,Int)]
triads n = [(a, b, c) | a <- [1..n], b <- [1..n], c <- [1..n], (c^2) == (a^2) + (b^2), a <= b]


indexOf :: Char -> String -> Maybe Int
indexOf a [] = Nothing
indexOf a (x:xs) = if x == a
                 then Just 1
                 else case indexOf a xs of
                        Nothing -> Nothing
                        Just y -> Just $ succ y

--positions :: Char -> String -> [Int]
--positions a [] = []
--positions a (x:xs) = if x == a
--                     then [1]
--                     else case positions a xs of

incAll :: [[Int]] -> [[Int]]
incAll = map (map (+ 1))

chuj lista = foldr (\x a -> x + a) 0 lista

cocat :: [[a]] -> [a]
cocat l = foldr(\x a -> x++a) [] l

nup :: Eq a => [a] -> [a]
nup [] = []
nup (x:xs) = x:nup(filter (/=x) xs)

data Tree a = Empty | Node a (Tree a) (Tree a) deriving (Ord)

freeTree :: Tree Char
freeTree =
    Node 'P'
        (Node 'O'
            (Node 'L'
                (Node 'N' Empty Empty)
                (Node 'T' Empty Empty)
            )
            (Node 'Y'
                (Node 'S' Empty Empty)
                (Node 'A' Empty Empty)
            )
        )
        (Node 'L'
            (Node 'W'
                (Node 'C' Empty Empty)
                (Node 'R' Empty Empty)
            )
            (Node 'A'
                (Node 'A' Empty Empty)
                (Node 'C' Empty Empty)
            )
        )
freeTree2 :: Tree Char
freeTree2 =
    Node 'P'
        (Node 'O'
            (Node 'L'
                (Node 'N' Empty Empty)
                (Node 'T' Empty Empty)
            )
            (Node 'Y'
                (Node 'S' Empty Empty)
                (Node 'A' Empty Empty)
            )
        )
        (Node 'C'
            (Node 'W'
                (Node 'C' Empty Empty)
                (Node 'R' Empty Empty)
            )
            (Node 'A'
                (Node 'A' Empty Empty)
                (Node 'C' Empty Empty)
            )
        )

instance Show a => Show (Tree a) where
   show Empty = "Empty"
   show (Node a b c) = "(" ++ show b ++ ")" ++ show a ++ "(" ++ show c ++ ")"

instance Eq a => Eq (Tree a) where
      (==) (Empty) (Empty) = True
      (==) (Empty) (Node a b c) = False
      (==) (Node a b c) (Empty) = False
      (==) (Node a b c) (Node d e f) = a == d && b == e && c == f

-- class  Functor f  where
--    fmap :: (a -> b) -> f a -> f b

instance Functor Tree where
  fmap f Empty = Empty
  fmap f (Node a b c) = Node (f a) (fmap f b) (fmap f c)
{--*Tree> fmap (+1) $ Node 1 Empty Empty
Node 2 Empty Empty-}


{-equalTrees :: Tree a -> Tree b -> IO()
equalTrees a b = if a == b
                 then putStrLn "equal Treees"
                 else putStrLn "unequal Trees"-}

toList :: Tree a -> [a]
toList Empty = []
toList (Node a b c) = toList(b)++[a]++toList(c)

--insert :: (Ord a) => a -> Tree a -> Tree a
--insert =
{-contains :: (Ord a) -> a -> Tree a -> Bool
fromList :: (Ord a) => [a] -> Tree a-}